<?php
	$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
	if($thumbnail){
	
	$width = 450;
	$height = 240;	

	$link = get_post_meta( get_the_ID(), 'mb_post_format_link', true );
?>
<div class="img_post">
	<div class="full_pattern"></div>
	<a href="<?php echo $link; ?>"  class="post_img ql_thumbnail_hover">
	<img src="<?php echo get_template_directory_uri(); ?>/framework/timthumb.php?src=<?php echo $thumbnail[0]; ?>&amp;h=<?php echo $height; ?>&amp;w=<?php echo $width; ?>" alt="<?php the_title(); ?>" />
	<span><i class="icon-eye-open"></i></span>
	<div class="clearfix"></div>
	</a>
	<time class="date_t" datetime="<?php the_time('c'); ?>" pubdate><?php the_time('F jS'); ?></time>
</div><!-- /img_post -->

<?php
	}
?>  