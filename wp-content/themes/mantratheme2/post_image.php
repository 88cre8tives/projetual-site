<?php
	$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
	if($thumbnail){
	
	$width = 450;
	$height = 240;
	$width_retina = $width * 2;
	$height_retina = $height * 2;
?>
<div class="img_post">
	<div class="full_pattern"></div>
	<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>" class="post_img ql_thumbnail_hover">
	<img src="<?php echo THEME_TIMTHUMB; ?>?src=<?php echo $thumbnail[0]; ?>&amp;h=<?php echo $height; ?>&amp;w=<?php echo $width; ?>" data-retinasrc="<?php echo THEME_TIMTHUMB; ?>?src=<?php echo $thumbnail[0]; ?>&amp;h=<?php echo $height_retina; ?>&amp;w=<?php echo $width_retina; ?>" alt="<?php the_title(); ?>" />
	<span><i class="icon-eye-open"></i></span>
	<div class="clearfix"></div>
	</a>
	<time class="date_t" datetime="<?php the_time('c'); ?>" pubdate><?php the_time('F jS'); ?></time>
</div><!-- /img_post -->

<?php
	}
?>  