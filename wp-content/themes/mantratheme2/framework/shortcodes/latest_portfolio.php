<?php
function latest_portfolio_f($atts, $content = null) {
		extract(shortcode_atts(array(
		"items" => '',
		"portfolio_name" => ''
	), $atts));

	


	$output = '<div class="row-fluid ql_latest_work">';

	query_posts(array('post_type' => $portfolio_name, 'posts_per_page' => $items)); 
		if (have_posts()) : while (have_posts()) : the_post();
		$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
		$width = 270;
		$height = 151;
		$alignment = "t";

            

	        $output .= '<div class="span3 ql_lw_item">';
	       
	        if ($thumbnail) {
	        	$output .= '<a href="'. get_permalink() .'" class="ql_thumbnail_hover"><img src="'. THEME_FRAMEWORK_URI .'/timthumb.php?src='. $thumbnail[0].'&amp;w='. $width .'&amp;h='. $height .'&amp;zc=1&amp;a='.$alignment.'" alt="thumbnail" /><span><i class="icon-eye-open"></i></span></a>';
	        }

	        $output .= '<a class="ql_info_footer hero_bck no-icon" href="'. get_permalink() .'">'. get_the_title() .'</a>
	                </div><!-- /ql_lw_item -->';
	


    		endwhile;

    	endif; 

    	wp_reset_query();
               
    $output .= '</div><!-- /row-fluid -->';







		
	return $output;
}
add_shortcode("latest_portfolio", "latest_portfolio_f");
?>