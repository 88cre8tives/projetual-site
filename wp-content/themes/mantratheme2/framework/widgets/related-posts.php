<?php
class widget_related_posts extends WP_Widget {
	
	function widget_related_posts() {
		$widget_ops = array('classname' => 'widget_related_posts', 'description' => 'Show related posts on a single post page' );
		$this->WP_Widget('widget_related_posts', 'Related Posts - Single Post - eneaa', $widget_ops);
	}

	function widget($args, $instance) {
		
			extract($args, EXTR_SKIP);
			
			$title = empty($instance['title']) ? '&nbsp;' : apply_filters('widget_title', $instance['title']);
			$number_posts = empty($instance['number_posts']) ? 4 : apply_filters('widget_number_posts', $instance['number_posts']);
			$thumbnail = $instance['thumbnail'] ? '1' : '0';
			
			
			?>
			<div class="related-post-widget">
				
		            	<?php
		            	if ( !empty( $title ) ) { echo "<h3 class='ql_title'>" . $title . "</h3>"; }; 
						?>
						<div class="row-fluid">
						<?php
						global $wp_query;
						$thePostID = $wp_query->post->ID;
						//get post categories and set $cat to first category
						$cat = get_the_category(); $cat = $cat[0];
						//set arguments for the get_posts function more options found at wordpress codex
						$args = array(
								'numberposts' => $number_posts,
								'offset' => '0',
								'category' => $cat->cat_ID, //calls category ID number
								'exclude'  => $thePostID
								);  ?>
						
						<?php
							global $post;
							$myposts = get_posts($args); //gets arguments from array above
							$counti = 0; //counter
							
							foreach($myposts as $post) : //loops through posts
								setup_postdata($post); //sets up posts
								
								
								?>
		                        	<div class="related_post span3">
		                        <?php
								
								
								
								$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
								if($thumbnail){
								?>

									<a href="<?php the_permalink(); ?>" class="ql_thumbnail_hover related_post_image">
		                            <span><i class="icon-eye-open"></i></span>
		                            <img src="<?php echo get_template_directory_uri(); ?>/framework/timthumb.php?src=<?php echo $thumbnail[0]; ?>&w=178&h=100&zc=1" alt="<?php the_title(); ?>" />
		                            </a>
		                            <?php
								}
									?>
		                            
		                            <h6 class="related_post_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
		                            </div>
		                            
								<?php 
								
								$counti++;
							endforeach; 
							
							// Reset the global $wp_query as this query will have stomped on it
							wp_reset_query();
							?>
						
					</div><!-- row-fluid -->
					<div class="clearfix"></div>
				</div>
			<?php
		}
		
		function update($new_instance, $old_instance) {
			$instance = $old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['number_posts'] = strip_tags($new_instance['number_posts']);
			$instance['thumbnail'] = !empty($new_instance['thumbnail']) ? 1 : 0;

			return $instance;
		}
		
		
		function form($instance) {

			
			$instance = wp_parse_args( (array) $instance, array( 'number_posts' => '', 'thumbnail' => '', 'title' => '') );
			
			$number_posts = strip_tags($instance['number_posts']);
			$title = strip_tags($instance['title']);
			$thumbnail = isset( $instance['thumbnail'] ) ? (bool) $instance['thumbnail'] : false;
			
			?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
			<p><label for="<?php echo $this->get_field_id('number_posts'); ?>">Number of posts to show: <input id="<?php echo $this->get_field_id('number_posts'); ?>" name="<?php echo $this->get_field_name('number_posts'); ?>" type="text" value="<?php echo esc_attr($number_posts); ?>" size="3" /></label></p>
			<p><label for="<?php echo $this->get_field_id('rss_link'); ?>">Disable Thumbnail?: <input id="<?php echo $this->get_field_id('thumbnail'); ?>" name="<?php echo $this->get_field_name('thumbnail'); ?>" type="checkbox" class="checkbox" <?php checked( $thumbnail ); ?> /></label></p>
			
			<?php
		}
}

register_widget('widget_related_posts');
?>