<?php
/*
Template Name: Galleria
*/
?>
<?php get_header(); ?>
		<?php get_template_part( "beforeloop", "fullwidth" ) ?> 
                
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            	<?php if ( post_password_required() ) : ?>
                                <div class="password_p">
                                    <?php the_content(); ?>
                                </div><!-- /password_p -->
            
                                <?php else : ?>
                        
                                
                                    <article id="post-<?php the_ID(); ?>" class="post">
                                    

                                    		<?php 
                                            //Get the page ID even if is the Blog Page
                                            global $wp_query;
                                            $page_id = $wp_query->get_queried_object_id();
                                            ?>
                                            
                                            <h2 class="line_title"><span><?php wp_title(''); ?></span></h2>

                                        	<?php 
                                        	//Remove the original Gallery Shortcode from the content
                                        	function remove_gallery($content) {
												$patron = '/\[(\[?)(gallery)(?![\w-])([^\]\/]*(?:\/(?!\])[^\]\/]*)*?)(?:(\/)\]|\](?:([^\[]*+(?:\[(?!\/\2\])[^\[]*+)*+)\[\/\2\])?)(\]?)/';
												return preg_replace($patron, '', $content);
											}
											add_filter( 'the_content', 'remove_gallery'); 
											//------------------------------------------------------<
                                        	?>


                                            <?php the_content(); ?>
                                            
                                            <?php

                                            if($wp_version >= 3.5){

                                            	$post_content = get_the_content();
												preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
												$array_id = explode(",", $ids[1]);


												if (count($array_id) > 1) {
													echo '<script src="'.get_template_directory_uri().'/js/galleria/galleria-1.2.7.min.js"></script>';
	                                                echo '<script src="'.get_template_directory_uri().'/js/galleria/themes/classic/galleria.classic.min.js"></script>';
													?>
														<script type="text/javascript">
														jQuery(document).ready(function($) {
															 $('#galleria').galleria({
																height:600,
																responsive:true,
																extend: function(options) {
																	this.bind('image', function(e) {
																		$(e.imageTarget).click(this.proxy(function() {
																		   this.openLightbox();
																		}));
																	});
																}
															});
														});
													</script>
													<?php
													echo '<div id="galleria">';
													$algo ="";
													foreach ($array_id as $image_id) {
															$image_attributes = wp_get_attachment_image_src( $image_id );
															$meta_info = wp_get_attachment_metadata($image_id);
															
															echo '<a rel="'. wp_get_attachment_url($image_id).'" href="'. wp_get_attachment_url($image_id).'"><img src="'.get_template_directory_uri().'/framework/timthumb.php?src='.wp_get_attachment_url($image_id).'&amp;w=750" alt="'.$meta_info[image_meta][caption].'" title="'.get_post($image_id)->post_excerpt.'"></a>';
																
													}//foreach	

													echo '</div>';
												}//count


                                            }else{

                                            
												$args = array(
													'order'          => 'ASC',
													'post_type'      => 'attachment',
													'post_parent'    => $post->ID,
													'post_mime_type' => 'image',
													'post_status'    => null,
													'numberposts'    => -1,
												);
												$attachments = get_posts($args);
												if (count($attachments) > 1) {
													echo '<script src="'.get_template_directory_uri().'/js/galleria/galleria-1.2.5.min.js"></script>';
	                                                echo '<script src="'.get_template_directory_uri().'/js/galleria/themes/classic/galleria.classic.min.js"></script>';
												?>
													<script type="text/javascript">
													jQuery(document).ready(function($) {
														 $('#galleria').galleria({
															width:900,
															height:600,
															extend: function(options) {
																this.bind('image', function(e) {
																	$(e.imageTarget).click(this.proxy(function() {
																	   this.openLightbox();
																	}));
																});
															}
														});
													});
												</script>
													<?php
													/*
													echo '<div id="galleria">';
													foreach ($attachments as $attachment) {
														$meta_info = wp_get_attachment_metadata($attachment->ID);
														echo '<a rel="'. wp_get_attachment_url($attachment->ID).'" href="'. wp_get_attachment_url($attachment->ID).'"><img src="'.get_template_directory_uri().'/framework/timthumb.php?src='.wp_get_attachment_url($attachment->ID).'&amp;w=750" alt="'.$attachment->post_content.'" title="'.$attachment->post_title.'"></a>';
															
													}//foreach	
													echo '</div>';
													*/
													
													
													echo '<div id="galleria">';
													foreach ($attachments as $attachment) {
														$meta_info = wp_get_attachment_metadata($attachment->ID);
														echo '<a rel="'. wp_get_attachment_url($attachment->ID).'" href="'. wp_get_attachment_url($attachment->ID).'"><img src="'.get_template_directory_uri().'/framework/timthumb.php?src='.wp_get_attachment_url($attachment->ID).'&amp;w=750" alt="'.$meta_info[image_meta][caption].'" title="'.$meta_info[image_meta][title].'"></a>';
															
													}//foreach	
													echo '</div>';
													
												}//if count		


											}//if wp_version									
											?>
                        
                                        
                                        
                                            <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
                                            
                                            <div class="clear"></div>
                                           
                                     </article>
                                     
                            	<?php endif; //password ?>
                                
                        
                            <?php endwhile; else: ?>
                        
                                
                                    <article>
                                        <p>Sorry, no posts matched your criteria.</p>
                                    </article>
                               
                        
                            <?php endif; ?>
                    
                          <?php get_template_part( "afterloop", "fullwidth" ) ?> 

<?php get_footer(); ?>