    <?php

            global $Theme;
        
        
            //Get the page ID even if is the Blog Page
            global $wp_query;
            $page_id = $wp_query->get_queried_object_id();
                        
            $background_image = get_post_meta($page_id, 'mb_background_image', true);
            $src = wp_get_attachment_image_src($background_image, 'full');
            
            //Audio---------------------------------
            $background_audio = get_post_meta($page_id, 'mb_background_audio', true);
            $background_audio_format = get_post_meta($page_id, 'mb_background_audio_format', true);
            

            $background_video = get_post_meta($page_id, 'mb_background_video', true);//KEEP <-------------





            if($background_video != "" AND !is_page_template('home-page-slider.php')){
            
                $video_autostart = of_get_option('video_autostart'); 
                $video_controlbar = of_get_option('video_controlbar'); 
                $video_stretching = of_get_option('video_stretching'); 
                $video_bufferlength = of_get_option('video_bufferlength'); 
                $video_repeat = of_get_option('video_repeat'); 


                echo "<div class='home_video_wrapper'>";
                //If is a Vimeo Video----------------------------------------------
                if (preg_match("/vimeo/i", $background_video)) {
                    
                    require_once (THEME_FULLSCREEN . "/vimeo.php");
                    
                } else if(preg_match("/youtube/i", $background_video) || preg_match("/youtu/i", $background_video)) {

                    require_once (THEME_FULLSCREEN . "/youtube.php");

                }else{//If is another type of video---------------------------------------------

                    require_once (THEME_FULLSCREEN . "/video.php");

                }//End if vimeo
                echo "</div>";
                
            }else{ //if $background_video (if not video)
                

                //The Gallery Template has her own fullscreen system
            if(!is_page_template('gallery-fullscreen.php')){
                if(of_get_option('site_layout') == "wrap"){
                    get_template_part( "slider-flexslider", "home-page" );

                }else{
                //Load the Fullscreen Image system
                require_once (THEME_FULLSCREEN . "/image.php");
                 

                ?>
                
            
            
                <!--Navigation-->
                <ul id="slide-list"></ul>

                <div id="controls" class="load-item">

                    <ul>
                        <!--<li><a href="#" id="ss_play_button" class="ss_button"><i class="icon-pause"></i> <i class="icon-play"></i></a></li>-->
                        <!--<li><a href="#" id="ss_tray_button" class="ss_button"><i class="icon-th"></i></a></li>-->
                        <!-- <li><a href="#" id="ss_fullscreen_button" class="ss_button"><i class="icon-resize-full"></i> <i class="icon-resize-small"></i></a></li> -->
                        <li><a href="#" id="ss_nextslide_button" class="ss_button"><i class="icon-chevron-right"></i></a></li>
                        <li><a href="#" id="ss_prevslide_button" class="ss_button"><i class="icon-chevron-left"></i></a></li>
                        
                    </ul>
                    <div class="clearfix"></div>
                

                </div><!-- /controls-->
                <?php
                }//if wrap
            
            }//if not fullscreen gallery












                /*
                *If there isn't a Background Video to show, let's show images background
                */
                
                //If there is an audio file set play it
                if($background_audio != ""){
                    //Audio Player-------------------------------
                    require_once (THEME_FULLSCREEN . "/audio_player.php");
                    
                }else{ //End if Audio ?>

                    <script type="text/javascript">
                    /* <![CDATA[ */
                    jQuery(document).ready(function($) { 
                        $("#jp_interface_1").hide();
                        $("#jp_play_pause").parent().hide();
                    });
                    /* ]]> */
                    </script>
                <?php
                }//End else $background_video
                            
            }//Else Video



            ?>







    <?php 
    $tagline = of_get_option('tagline'); 
    if($tagline){ 
    ?>
    <div class="ql_tagline hero_bck">
            <h2><?php echo stripslashes($tagline); ?></h2>
    </div>
    <?php } ?>

    <div class="container"><!-- /container -->




       



        <div id="container">

            <div id="main" role="main" class="">
                                
                    
                <section id="content">
