<?php get_header(); ?>
		<?php get_template_part( "beforeloop", "archive" ) ?> 


						<?php if (have_posts()) : ?>
						
						

                                <?php if(!is_single()){ ?>
                                        <?php $post = $posts[0]; // hack: set $post so that the_date() works ?>
                                        <?php if (is_category()) { ?>
                                        
                                        <h2><?php printf(__('Archive for the &ldquo;%s&rdquo; Category', 'eneaa'), single_cat_title('', false));  ?></h2>
                            
                                        <?php } elseif(is_tag()) { ?>
                                        <h2><?php printf(__('Posts Tagged &ldquo;%s&rdquo;', 'eneaa'), single_tag_title('', false));  ?></h2>
                            
                                        <?php } elseif (is_day()) { ?>
                                        <h2><?php printf(__('Archive for %s', 'eneaa'), get_the_time('F jS, Y'));  ?></h2>
                            
                                        <?php } elseif (is_month()) { ?>
                                        <h2><?php printf(__('Archive for %s', 'eneaa'), get_the_time('F Y'));  ?></h2>
                            
                                        <?php } elseif (is_year()) { ?>
                                        <h2><?php printf(__('Archive for %s', 'eneaa'), get_the_time('Y'));  ?></h2>
                            
                                        <?php } elseif (is_author()) { ?>
                                        <h2><?php _e('Author Archive', 'eneaa');  ?></h2>
                            
                                        <?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
                                        <h2><?php _e('Blog Archives', 'eneaa');  ?></h2>
                            
                                        <?php } ?>
                                    
                                    <?php } ?>

                                <div class="clearfix"></div>

                 


						
                        <?php while (have_posts()) : the_post(); ?>
                
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >

                                <?php 
                                if(!get_post_format()) {
                                       //Display the Post Image by default
                                       get_template_part( "post_image", "archive" );
                                } else {
                                       get_template_part('format', get_post_format());
                                }
                                ?>

                                <div class="post_content">
                                    <div class="entry_wrap">
                                        <div class="entry">

                                            <div class="post_title">
                                                <?php get_template_part( "post_title", "archive" ); ?>
                                            </div><!-- /post_title -->


                                            <?php the_content(__('Read more...', 'eneaa')); ?>

                                            <div class="clearfix"></div>

                                             <?php get_template_part( "meta", "archive" ); ?>




                                        </div><!-- /entry -->
                                    </div><!-- /entry_wrap -->

                                </div><!-- /post_content -->
  
                            </article>
                
                            <?php endwhile; ?>
                
                            <?php else : ?>
                
                            <article>
                                <h3><?php _e('Not Found','eneaa'); ?></h3>
                        		<p><?php _e('Sorry, but the requested resource was not found on this site.','eneaa'); ?></p>
                                <div class="clear"></div>
                            </article>
                
                            <?php endif; ?>
            

                  <?php get_template_part( "afterloop", "archive" ) ?> 

	<?php get_footer(); ?>
