<?php
/*
Template Name: Flex Gallery
*/
?>
<?php get_header(); ?>
		<?php get_template_part( "beforeloop", "fullwidth" ) ?> 
                
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            	<?php if ( post_password_required() ) : ?>
                                <div class="password_p">
                                    <?php the_content(); ?>
                                </div><!-- /password_p -->
            
                                <?php else : ?>
                        
                                
                                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
                                    
											<?php 
                                            //Get the page ID even if is the Blog Page
                                            global $wp_query;
                                            $page_id = $wp_query->get_queried_object_id();
                                            ?>
                                            
                                            <h2 class="line_title"><span><?php wp_title(''); ?></span></h2>
                                        	
                                            <?php 
                                             //Remove the original Gallery Shortcode from the content
                                        	function remove_gallery($content) {
												$patron = '/\[(\[?)(gallery)(?![\w-])([^\]\/]*(?:\/(?!\])[^\]\/]*)*?)(?:(\/)\]|\](?:([^\[]*+(?:\[(?!\/\2\])[^\[]*+)*+)\[\/\2\])?)(\]?)/';
												return preg_replace($patron, '', $content);
											}
											add_filter( 'the_content', 'remove_gallery'); 
											//------------------------------------------------------<

                                            the_content(); 
                                            ?>


                                            
                                            
		                                    <?php 

		                                    if($wp_version >= 3.5){


		                                    	$post_content = get_the_content();
												preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
												$array_id = explode(",", $ids[1]);

												$width = 1000;
				                                $width_retina = 2000;

				                                $height = 666;
				                                $height_retina = 1333;

                                            	if (count($array_id) > 1) {

                                            	?>

                                            		<div class="flex-container">
												            	<div class="flexslider">
																  <ul class="slides">
																<?php

																foreach ($array_id as $image_id) {
																	$meta_info = wp_get_attachment_metadata($image_id);
				
																	$image_attributes = wp_get_attachment_image_src( $image_id );
																	
																	echo '<li>';
																	echo'<img src="'.THEME_TIMTHUMB.'?src='.wp_get_attachment_url($image_id).'&amp;w='. $width .'&amp;h='. $height .'"  data-retinasrc="'.THEME_TIMTHUMB.'?src='.wp_get_attachment_url($image_id).'&amp;w='. $width_retina .'&amp;h='. $height_retina .'" alt="'.$image_attributes->post_title.'" />';
																	if(get_post($image_id)->post_excerpt){ echo '<div class="flex-caption">'.get_post($image_id)->post_excerpt.'</div>';}
																	echo'</li>';
																	
																}//foreach	


																?>
																	</ul>
																</div><!-- /flexslider -->
															</div><!-- /flex-container -->




												            	<div class="flex_carousel">
																  <ul class="slides">
																<?php
																foreach ($array_id as $image_id) {
																	$meta_info = wp_get_attachment_metadata($attachment->ID);
																	$image_attributes = wp_get_attachment_image_src( $image_id );
																	
																	echo '<li><img src="'.THEME_TIMTHUMB.'?src='.wp_get_attachment_url($image_id).'&amp;w=210&amp;h=118"  alt="'.$image_attributes->post_title.'" /></li>';
																		
																}//foreach	
																?>
																	</ul>
																</div><!-- /flex_carousel -->

												<?php

                                            	}//if count

                                            }else{




														$args = array(
															'order'          => 'ASC',
															'post_type'      => 'attachment',
															'post_parent'    => $post->ID,
															'post_mime_type' => 'image',
															'post_status'    => null,
															'numberposts'    => -1,
														);
														$attachments = get_posts($args);
														if (count($attachments) > 1) {
														?>
										                	<div class="flex-container">
												            	<div class="flexslider">
																  <ul class="slides">
																<?php
																foreach ($attachments as $attachment) {
																	$meta_info = wp_get_attachment_metadata($attachment->ID);
																	
																	echo '<li><img src="'.get_template_directory_uri().'/framework/timthumb.php?src='.wp_get_attachment_url($attachment->ID).'&amp;w=940&amp;h=528"  alt="'.$attachment->post_title.'" /></li>';
																		
																}//foreach	
																?>
																	</ul>
																</div><!-- /flexslider -->
															</div><!-- /flex-container -->




												            	<div class="flex_carousel">
																  <ul class="slides">
																<?php
																foreach ($attachments as $attachment) {
																	$meta_info = wp_get_attachment_metadata($attachment->ID);
																	
																	echo '<li><img src="'.get_template_directory_uri().'/framework/timthumb.php?src='.wp_get_attachment_url($attachment->ID).'&amp;w=210&amp;h=118"  alt="'.$attachment->post_title.'" /></li>';
																		
																}//foreach	
																?>
																	</ul>
																</div><!-- /flex_carousel -->
															






															
															<?php
														}//if count		

											} //if wp_version										
													?>
							                        
                                        
                                        
                                            <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
                                            
                                            <div class="clear"></div>
                                           
                                     </article>
                                     
                            	<?php endif; //password ?>
                                
                        
                            <?php endwhile; else: ?>
                        
                                
                                    <article>
                                        <p>Sorry, no posts matched your criteria.</p>
                                    </article>
                               
                        
                            <?php endif; ?>
                    
                          <?php get_template_part( "afterloop", "fullwidth" ) ?> 

<?php get_footer(); ?>