<?php get_header(); ?>
		<?php get_template_part( "beforeloop", "search" ) ?> 

					<?php if (have_posts()) : ?>
        
                    
                        <?php 
                                            //Get the page ID even if is the Blog Page
                                            global $wp_query;
                                            $page_id = $wp_query->get_queried_object_id();
                                            ?>
                                            
                                            <h2 class="line_title"><span><?php wp_title(''); ?></span></h2>
        
                        <?php while (have_posts()) : the_post(); ?>
        
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
     

                                



                                        <?php get_template_part( "post_title", "search" ); ?>

                                        <?php the_excerpt(); ?>

                                        <?php
                                        get_template_part( "meta", "search" );
                                        ?> 


                                        

                                  
      
                            </article>
        
                        <?php endwhile; ?>
        
                        
                   
                    <?php include (TEMPLATEPATH . '/framework/nav.php' );?>
        
                    <?php else : ?>
        
                    <article>
                        <h2 class="post_title">Not Found</h2>
                        <p>Sorry, but the requested resource was not found on this site.</p>
                        <div class="clear"></div>
                    </article>
        
                    <?php endif; ?>
                    
              <?php get_template_part( "afterloop", "search" ) ?> 

<?php get_footer(); ?>