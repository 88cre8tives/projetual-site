//Portfolio Script 1.0
//Author: Nico Andrade
//http://www.eneaa.com
jQuery(document).ready(function($) {





	

	






	
	//Test if it's a touch device
	if ('ontouchstart' in document) {
		$('body').removeClass('no-touch');
	}


	var $portfolio_viewport = $("#portfolio_viewport");
	var p_viewport_options = {
			touchbehavior: true,
			cursorwidth: "3",
			bouncescroll: true,
			cursorcolor:"#fff",
		}
	//Scroll for portfolio items
	$portfolio_viewport.niceScroll(p_viewport_options);

	var $container_isotope = $('#portfolio_container');
	
	//Set height for loading gif
	$container_isotope.addClass("ql_height");

	$container_isotope.imagesLoaded( function( $images, $proper, $broken ) {

		//Remove preloaded
				$(".preloader").css('display', 'none');
				$container_isotope.removeClass("notloadyet");


				$container_isotope.isotope({
					// options
					itemSelector : '.portfolio_item',
					layoutMode : 'masonryHorizontal',
					resizable : false,
					transformsEnabled : true,

					getSortData : {
						year : function ( $elem ) {
							return parseInt( $elem.attr('data-year'),10);
						}
					}

					});//isotope

				//$container_isotope.isotope({ sortBy : 'year' });


				//Resize Items at start
				resizeContainer();
				$container_isotope.isotope('reLayout'); 

				//Move Filter lists
				$('.filter_list').appendTo("#filter_lists");

				// filter items when filter link is clicked
				$('.filter_list a').click(function(){
					var selector = $(this).attr('data-filter');
					$container_isotope.isotope({ filter: selector });
					var $parent = $(this).parents(".filter_list");
					$parent.find(".active").removeClass('active');
					$(".filter_list").not($parent).find("li").removeClass('active').first().addClass("active");
					$(this).parent().addClass("active");
					return false;
				});



				//Resize Isotope on widow resize.
				function resizeContainer(){

					var window_height = $(window).height();

					if (window_height > 858) {
						$container_isotope.height(710);
					}else if(window_height > 660){
						$container_isotope.height(480);
					}else{
						$container_isotope.height(250);
					};


				}



				$(window).on("debouncedresize", function( event ) {
					 
					resizeContainer();
					$container_isotope.isotope('reLayout');
					setTimeout(function(){ $container_isotope.isotope('reLayout'); $portfolio_viewport.niceScroll(p_viewport_options).resize();	 }, 500);
					
				});
				

				

				
				//Add class for Flip effect
				$(".no-touch .portfolio_item").hover( function(){
					$(this).children('a').addClass('animated flipInY');
				},function(){
					$(this).children('a').removeClass('animated flipInY');
				});
				








				



	});//images loaded






	//Scroll for portfolio items
			$(".post_content.post_portfolio").niceScroll({
				touchbehavior: true,
				cursorwidth: "3",
				bouncescroll: true,
				cursorcolor:"#fff",
			});



			var $portfolioImgs_container = $(".portfolio_list_img");

			$portfolioImgs_container.imagesLoaded( function( $images, $proper, $broken ) {

				var $portfolioImgs = $(".portfolio_list_img ul li");
				
				var $portfolio_entry_wrap = $(".single-portfolio .entry_wrap");

				//Resize on widow resize.
					function resizePortfolioImgs(){

						var window_height = $(window).height();
						var window_width = $(window).width();
						window_height = window_height * 0.75;
						
						$portfolioImgs.height(window_height);
						$portfolioImgs_container.height(window_height);
						$("#portfolio_viewport .post_content").height(window_height);
						$portfolio_entry_wrap.height($(".entry").height());

		
						

						var t_width = 0;
						var img_ratio = 0;
						var img_height = 0;
						if (window_width > 767) {
				
							$.each($portfolioImgs, function(key, item) {
								//Images
								t_width += $(item).width();
								img_ratio = $(item).children("img").attr("data-aspectratio");
								img_height = $(item).children("img").height();
								$(item).children("img").width(img_ratio * img_height);
								//Iframe videos
								img_ratio = $(item).find(".portfolio_video").attr("data-aspectratio");
								$(item).find(".portfolio_video").height(window_height);
								img_height = window_height;
								$(item).find(".portfolio_video").width(img_ratio * img_height);
							});
						}else{
							//$portfolio_viewport.niceScroll(p_viewport_options).remove();

						};

						$("#portfolio_viewport .post").width(t_width);

					}
					resizePortfolioImgs();
				

				


				$(window).on("debouncedresize", function( event ) {
						 
						resizePortfolioImgs();
						
				});







			});//images loaded








});