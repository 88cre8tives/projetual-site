

<?php get_header("simple"); ?>
	

		<?php get_template_part( "beforeloop", "single-portfolio" ) ?> 
                
                	<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
                    	<?php if ( post_password_required() ) : ?>
                                <div class="password_p">
                                    <?php the_content(); ?>
                                </div><!-- /password_p -->
            
                                <?php else : ?>
                                
                                <body style="background-color: transparent; margin-top: -32px">
                                	
		                            <article id="post-<?php the_ID(); ?>" <?php post_class('portfolio-post') ?> >

		                            	<div class="post_content post_portfolio" >

					                        <div class="entry_wrap">

					                            <div class="entry">
					                                <h1><?php the_title(); ?></h1>
					                                <span class="ql_line"></span>
					                                
					                                
					                                
					                                <div class="clearfix"></div>
						                        	<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

					                                <span class="ql_line"></span>



						                    


								                    <div class="portfolioItem_sidebar">
								                       <ul class="unstyled custom_cats">
								                        	<?php
								                        	$portfolio_info_client = get_post_meta(get_the_ID(), 'mb_portfolio_info_client', true);
															$portfolio_info_url = get_post_meta(get_the_ID(), 'mb_portfolio_info_url', true);
															$portfolio_info_year = get_post_meta(get_the_ID(), 'mb_portfolio_info_year', true);
															$portfolio_info_agency = get_post_meta(get_the_ID(), 'mb_portfolio_info_agency', true);
															?>
								                            <?php if($portfolio_info_client){ echo "<li><strong>Client:</strong> ".$portfolio_info_client."</li>";} ?>
								                            <?php if($portfolio_info_url){ echo "<li><strong>URL:</strong> ".$portfolio_info_url."</li>";} ?>
								                            <?php if($portfolio_info_year){ echo "<li><strong>Year:</strong> ".$portfolio_info_year."</li>";} ?>
								                            <?php if($portfolio_info_agency){ echo "<li><strong>Agency:</strong> ".$portfolio_info_agency."</li>";} ?>
								                        </ul>
								                        <?php foreach((get_the_category()) as $category){  
								                        	echo '<a href="'.get_category_link($category->cat_ID).'" class="min_tag">'.$category->cat_name . '</a> ';
								                        
								                    	} ?>

								                        <?php the_tags('<ul class="fancy_tags"><li>','</li><li>','</li></ul>'); ?>
								                        
								                       

								                        <div class="clearfix"></div>


								                    </div><!-- /portfolioItem_sidebar -->
					                                
					                                
					                             

					                            	<div class="clearfix"></div>

					                        	</div><!-- /entry -->
					                                               

					                    	</div><!-- /entry_wrap -->

					                        

					                    </div><!-- /post_content -->



					              


                              					<?php 
                                        	//Remove the original Gallery Shortcode from the content
                                        	function remove_gallery($content) {
												$patron = '/\[(\[?)(gallery)(?![\w-])([^\]\/]*(?:\/(?!\])[^\]\/]*)*?)(?:(\/)\]|\](?:([^\[]*+(?:\[(?!\/\2\])[^\[]*+)*+)\[\/\2\])?)(\]?)/';
												return preg_replace($patron, '', $content);
											}
											add_filter( 'the_content', 'remove_gallery'); 
											//------------------------------------------------------<
                                        	?>

                                            <div style="position: relative; margin-left: 10%; margin-right: 10%; margin-top: -8px; "><?php the_content(); ?></div>
                                            <?php

                                            if($wp_version >= 3.5){

                                            	$post_content = get_the_content();
												preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);



								global $wpdb;
														$portfolio_images = get_post_meta( get_the_ID(), 'mb_portfolio_images', false );
														
														if($portfolio_images){
															$portfolio_images = implode( ',' , $portfolio_images );

															// Re-arrange portfolio_images with 'menu_order'
															$portfolio_images = $wpdb->get_col( "
															    SELECT ID FROM {$wpdb->posts}
															    WHERE post_type = 'attachment'
															    AND ID in ({$portfolio_images})
															    ORDER BY menu_order ASC
															" );}


												
					

												
												

												//$array_id = explode(",", $ids[1]);
										
												$array_id = $portfolio_images;


												if (count($array_id) > 1) {
													echo '<script src="'.get_template_directory_uri().'/js/galleria/galleria-1.2.7.min.js"></script>';
	                                                echo '<script src="'.get_template_directory_uri().'/js/galleria/themes/classic/galleria.classic.min.js"></script>';
													?>
														<script type="text/javascript">
														jQuery(document).ready(function($) {
															 $('#galleria').galleria({
																height:400,
																width: 750,
																responsive:true,
																extend: function(options) {
																	this.bind('image', function(e) {
																		$(e.imageTarget).click(this.proxy(function() {
																		   this.openLightbox();
																		}));
																	});
																}
															});
														});
													</script>
													<?php
													echo '<div id="galleria">';
													$algo ="";
													foreach ($array_id as $image_id) {
															$image_attributes = wp_get_attachment_image_src( $image_id );
															$meta_info = wp_get_attachment_metadata($image_id);
															
															echo '<a rel="'. wp_get_attachment_url($image_id).'" href="'. wp_get_attachment_url($image_id).'"><img src="'.get_template_directory_uri().'/framework/timthumb.php?src='.wp_get_attachment_url($image_id).'&amp;w=750" alt="'.$meta_info[image_meta][caption].'" title="'.get_post($image_id)->post_excerpt.'"></a>';
																
													}//foreach	

													echo '</div>';
												}//count


                                            }else{

                                            
												$args = array(
													'order'          => 'ASC',
													'post_type'      => 'attachment',
													'post_parent'    => $post->ID,
													'post_mime_type' => 'image',
													'post_status'    => null,
													'numberposts'    => -1,
												);
												$attachments = get_posts($args);
												if (count($attachments) > 1) {
													echo '<script src="'.get_template_directory_uri().'/js/galleria/galleria-1.2.5.min.js"></script>';
	                                                echo '<script src="'.get_template_directory_uri().'/js/galleria/themes/classic/galleria.classic.min.js"></script>';
												?>
													<script type="text/javascript">
													jQuery(document).ready(function($) {
														 $('#galleria').galleria({
															width:600,
															height:400,
															extend: function(options) {
																this.bind('image', function(e) {
																	$(e.imageTarget).click(this.proxy(function() {
																	   this.openLightbox();
																	}));
																});
															}
														});
													});
												</script>
													<?php
													/*
													echo '<div id="galleria">';
													foreach ($attachments as $attachment) {
														$meta_info = wp_get_attachment_metadata($attachment->ID);
														echo '<a rel="'. wp_get_attachment_url($attachment->ID).'" href="'. wp_get_attachment_url($attachment->ID).'"><img src="'.get_template_directory_uri().'/framework/timthumb.php?src='.wp_get_attachment_url($attachment->ID).'&amp;w=750" alt="'.$attachment->post_content.'" title="'.$attachment->post_title.'"></a>';
															
													}//foreach	
													echo '</div>';
													*/
													
													
													echo '<div id="galleria">';
													foreach ($attachments as $attachment) {
														$meta_info = wp_get_attachment_metadata($attachment->ID);
														echo '<a rel="'. wp_get_attachment_url($attachment->ID).'" href="'. wp_get_attachment_url($attachment->ID).'"><img src="'.get_template_directory_uri().'/framework/timthumb.php?src='.wp_get_attachment_url($attachment->ID).'&amp;w=750" alt="'.$meta_info[image_meta][caption].'" title="'.$meta_info[image_meta][title].'"></a>';
															
													}//foreach	
													echo '</div>';
													
												}//if count		


											}//if wp_version									
											?>
                        
                                        
                                        
                                            <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
                                            
										



		                            </article>


		                                    
		                                    
										
									
									
									<?php // comments_template(); ?>
									
                    
        		<?php endif; //password ?>
            <?php endwhile; 
			
			//include (TEMPLATEPATH . '/framework/nav.php' );
			
			else: ?>
        
                    <article>
                        <p><?php _e('Sorry, but the requested resource was not found on this site.','eneaa'); ?></p>
                    </article>
        
            <?php endif; ?>
            
                  <?php get_template_part( "afterloop", "single-portfolio" ) ?> 

<?php //get_footer(); ?>
</body>
