<div class="metadata hero_border">
	<ul>
		<li class="meta_date"><i class="icon-calendar"></i> <time datetime="<?php the_time('c'); ?>" pubdate><?php the_time('F jS, Y'); ?></time></li>
        <li class="meta_author"><i class="icon-user"></i> <?php the_author() ?></li>
        <li class="meta_category"><i class="icon-folder-open"></i> <?php the_category(', ') ?></li>
        <li class="meta_comments"><i class="icon-comment"></i> <?php comments_popup_link(__('No Comments', 'eneaa'), __('1 Comment', 'eneaa'), __('% Comments', 'eneaa')); ?></li>
    </ul>
    <div class="clearfix"></div>
</div><!-- /metadata -->
			            	
			            		