<?php
function latest_blog_f($atts, $content = null) {
		extract(shortcode_atts(array(
		"articles" => ''
	), $atts));

	







	$output = '<div class="row-fluid ql_latest_blog">';

	query_posts(array('posts_per_page' => $articles)); 
		if (have_posts()) : while (have_posts()) : the_post();
		$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
		$width = 270;
		$height = 151;



		
        $output .= '<div class="span3 ql_lblog_item">';

        if(!get_post_format()) {



            if ($thumbnail) {
        		$output .= '<a href="'. get_permalink() .'" class="ql_thumbnail_hover"><img src="'. THEME_FRAMEWORK_URI .'/timthumb.php?src='. $thumbnail[0].'&amp;w='. $width .'&amp;h='. $height .'&amp;zc=1" alt="thumbnail" /><span><i class="icon-eye-open"></i></span></a>';
        	}






        } elseif (get_post_format() == "gallery") {


			$images = get_post_meta( get_the_ID(), 'mb_post_format_images', false );
			
			$output .= '<div class="flex-container">
			            	
			    	<div class="flexslider">
			            <div class="full_pattern"></div>
					  <ul class="slides">';
			
						foreach ( $images as $att )
						{
						    // Get image's source based on size, can be 'thumbnail', 'medium', 'large', 'full' or registed post thumbnails sizes
						    $src = wp_get_attachment_image_src( $att, 'full' );
						    $src = $src[0];

						    //To show a title in the lightbox you need to edit the image and add an ALT text
						    $alt_text = get_post_meta($att, '_wp_attachment_image_alt', true);

						   
						                 	$output .= '<li>
						                 		<a href="'. $src .'" rel="prettyPhoto[1]" title="'. $alt_text .'">
						                 		<img src="'. THEME_FRAMEWORK_URI .'/timthumb.php?src='. $src .'&amp;w='. $width .'&amp;h='. $height .'&amp;zc=1" alt="'. get_the_title(). '" class="slider_img"  />
						                 		</a>
						                 	</li>';
						    
						}
			
			$output .= '</ul>
				</div><!-- /flexslider -->
			</div><!-- /flex-container -->';









        } elseif (get_post_format() == "audio") {


			$audio_mp3 = get_post_meta( get_the_ID(), 'mb_post_format_audio_mp3', true );
			$audio_ogg = get_post_meta( get_the_ID(), 'mb_post_format_audio_ogg', true );
			
			$output .= '<div class="default_format audio_wrap inner_shadow_format">
				<div class="clearfix"></div>
			<audio class="AudioPlayerV1" preload="none"  data-fallback= "'. get_template_directory_uri().'/js/AudioPlayerV1.swf">';
			    if($audio_mp3){
			    	$output .= '<source type="audio/mpeg" src="'.$audio_mp3.'"></source>';
			    }
			    if($audio_ogg){

			    	$output .= '<source type="audio/ogg" src="'. $audio_ogg.'"></source>';
			    }
			$output .= '</audio>
			</div><!-- /audio_wrap -->';








		} elseif (get_post_format() == "video") {


		
			$video_URL = get_post_meta( get_the_ID(), 'mb_post_format_video', true );


			//If is a Vimeo Video----------------------------------------------
					if (preg_match("/vimeo/i", $video_URL)) {
						
						//Get the path of the URL
						$vimeo_video = parse_url($video_URL, PHP_URL_PATH);
						
						//Remove "/" if is in the URL at the end or beggining
						$vimeo_video = trim($vimeo_video, "/");	
						
					
					$output .= '<div class="videoWrapper">
			            <iframe class="post_video" height="433" src="http://player.vimeo.com/video/'. $vimeo_video.'?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
			        </div>';
			        
					}else if(preg_match("/youtube/i", $video_URL)){
						//Get the path of the URL
						parse_str( parse_url( $video_URL, PHP_URL_QUERY ), $youtube_video );
					
					$output .= '<div class="videoWrapper">
						<iframe class="post_video" width="100%" height="433" src="http://www.youtube.com/embed/'. $youtube_video['v'] .'" frameborder="0" allowfullscreen></iframe>
					</div>';

					}else if( preg_match("/youtu/i", $video_URL)){ 
						//Get the path of the URL
						$youtube_video = parse_url($video_URL, PHP_URL_PATH);
						
						//Remove "/" if is in the URL at the end or beggining
						$youtube_video = trim($youtube_video, "/");	
					
					$output .= '<div class="videoWrapper">
						<iframe class="post_video" width="100%"  height="433" src="http://www.youtube.com/embed/'. $youtube_video .'" frameborder="0" allowfullscreen></iframe>
					</div>';
					}else{$output .= '

					<div class="flowplayer" data-swf="'. get_template_directory_uri() .'/js/flowplayer.swf">
						<video autoplay>
							<source type="video/mp4" src="'. $video_URL .'"/>
						</video>
					</div>

					<script type="text/javascript"> 
					/* <![CDATA[ */
					jQuery(document).ready(function($) {  
						flowplayer.conf.embed = false;						
						$("#controls").hide();
					 });
					 /* ]]> */
					</script>';

					}












        } else {
        	$output .= '<a href="'. get_permalink() .'" class="ql_thumbnail_hover"><img src="'. THEME_FRAMEWORK_URI .'/timthumb.php?src='. $thumbnail[0].'&amp;w='. $width .'&amp;h='. $height .'&amp;zc=1" alt="thumbnail" /><span><i class="icon-eye-open"></i></span></a>';
           
        } 
        

                    
        $output .= '<div class="ql_lblog_text">
                        <h4><a href="'. get_permalink() .'">'. get_the_title() .'</a></h4>
                        <p>'.string_limit_words(get_the_excerpt(),25).'</p>
                        <a href="'. get_permalink() .'">'.__("Read more...", "eneaa").'</a>

                    </div><!-- /ql_lblog_text -->
                    <p class="ql_info_footer hero_bck"><i class="icon-calendar"></i> <time datetime="'. get_the_time("c").'" pubdate >'. get_the_time('F jS') .'</time></p>

                </div><!-- /ql_lblog_item -->';
	



    endwhile;

    endif; 

    wp_reset_query();
               
    $output .= '</div><!-- /row-fluid -->';























		
	return $output;
}
add_shortcode("latest_blog", "latest_blog_f");
?>