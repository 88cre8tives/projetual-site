<?php get_header(); ?>

		<?php get_template_part( "beforeloop", "page" ) ?> 
                    
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        
							<?php if ( post_password_required() ) : ?>
                            <div class="password_p">
                            	<?php the_content(); ?>
                            </div><!-- /password_p -->
        
                            <?php else : ?>
                        
                                
                                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
                                    
                                             <?php 
                                            //Get the page ID even if is the Blog Page
                                            global $wp_query;
                                            $page_id = $wp_query->get_queried_object_id();
                                            ?>
                                            
                                            <style type="text/css">
                                                iframe#iframe-spec {
                                                	width: 785px;
                                                	height: 550px;
                                                	border: none;
                                                	overflow: hidden;
                                                	background-color:transparent;
                                                }
                                                
                                                iframe#iframe-spec::-webkit-scrollbar {
                                                	display: none;
                                                }
                                            </style>
                                           
                                            <iframe ALLOWTRANSPARENCY="true" src="<?php echo get_site_url(); echo "?post_type=portfolio&p="; echo $_GET["projeto"]?>" name="janela" id="iframe-spec"></iframe>
                                        
                                        
                                            <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
                                            
                                            
                                            
                                            <div class="clearfix"></div>
                                            
                                    </article>
                                
                            <?php endif; //password ?>
                        
                        <?php endwhile; else: ?>
                    
                            
                                <article>
                                	
                        			<p><?php _e('Sorry, but the requested resource was not found on this site.','eneaa'); ?></p>
                                    
                                    <div class="clearfix"></div>
                                </article>
                           
                    
                        <?php endif; ?>
                        
                  <?php get_template_part( "afterloop", "page" ) ?> 

<?php get_footer(); ?>