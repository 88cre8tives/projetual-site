<?php
/*
Template Name: Home Page 
*/
?>
<?php get_header(); ?>


		<?php get_template_part( "beforeloop", "home-page" ) ?> 
        	
            <!--<h3 class="post_title"><?php wp_title(''); ?></h3>-->

          

            <?php
            //Flexslider
            //get_template_part( "slider", "flexslider" );
            global $Theme;
        
        
            //Get the page ID even if is the Blog Page
            global $wp_query;
            $page_id = $wp_query->get_queried_object_id();
                        
            $background_image = get_post_meta($page_id, 'mb_background_image', true);
            $src = wp_get_attachment_image_src($background_image, 'full');
            
            //Audio---------------------------------
            $background_audio = get_post_meta($page_id, 'mb_background_audio', true);
            $background_audio_format = get_post_meta($page_id, 'mb_background_audio_format', true);
            

            $background_video = get_post_meta($page_id, 'mb_background_video', true);//KEEP <-------------





            if($background_video != "" AND !is_page_template('home-page-slider.php')){
            
                $video_autostart = of_get_option('video_autostart'); 
                $video_controlbar = of_get_option('video_controlbar'); 
                $video_stretching = of_get_option('video_stretching'); 
                $video_bufferlength = of_get_option('video_bufferlength'); 
                $video_repeat = of_get_option('video_repeat'); 

                //If is a Vimeo Video----------------------------------------------
                if (preg_match("/vimeo/i", $background_video)) {
                    
                    require_once (THEME_FULLSCREEN . "/vimeo.php");
                    
                } else {//If is another type of video---------------------------------------------
                
                    require_once (THEME_FULLSCREEN . "/video.php");

                }//End if vimeo
                
            }else{ //if $background_video (if not video)
                
                /*
                *If there isn't a Background Video to show, let's show images background
                */
                
                //If there is an audio file set play it
                if($background_audio != ""){
                    //Audio Player-------------------------------
                    require_once (THEME_FULLSCREEN . "/audio_player.php");
                    
                }else{ //End if Audio ?>

                    <script type="text/javascript">
                    /* <![CDATA[ */
                    jQuery(document).ready(function($) { 
                        $("#jp_interface_1").hide();
                        $("#jp_play_pause").parent().hide();
                    });
                    /* ]]> */
                    </script>
            <?php
                }//End else $background_video
                            
            }//Else Video


            //The Gallery Template has her own fullscreen system
            if(!is_page_template('gallery-fullscreen.php')){
            
                //Load the Fullscreen Image system
                require_once (THEME_FULLSCREEN . "/image.php");
            
            }//if not fullscreen gallery

            ?>


            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                                <?php the_content(); ?>             
                        
                    <?php endwhile; else: ?>
                                <article>
                                    <p><?php _e('Sorry, but the requested resource was not found on this site.','eneaa'); ?></p>
                                    <div class="clear"></div>
                                </article>
                    <?php endif; ?>




                    <div id="widget_area_home">
       
                            <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('home-widgets')) : else : ?>
                            <?php endif; ?>
                            
                        <div class="clearfix"></div>
                    </div><!-- /widget_area_home-->
                    
              
        <?php  get_template_part( "afterloop", "fullwidth" ) ?> 

<?php get_footer(); ?>