<div class="post_title">
<?php
if (has_post_format("link")) { 
    $link = get_post_meta( get_the_ID(), 'mb_post_format_link', true );
  ?>
  <h2 class="ql_title"><a href="<?php echo $link; ?>" target="_blank" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">[Link] <?php the_title(); ?></a></h2>

<?php }else { ?>

  <h2><?php the_title(); ?></h2>
  <span class="ql_line"></span>

<?php } ?>
</div><!-- /post_title -->