<?php
/*
Template Name: Fullscreen Gallery
*/
?>
<?php get_header(); ?>
<!--Navigation-->
                <ul id="slide-list"></ul>

                <div id="controls" class="load-item">

                    <ul>
                        <!--<li><a href="#" id="ss_play_button" class="ss_button"><i class="icon-pause"></i> <i class="icon-play"></i></a></li>-->
                        <!--<li><a href="#" id="ss_tray_button" class="ss_button"><i class="icon-th"></i></a></li>-->
                        <!-- <li><a href="#" id="ss_fullscreen_button" class="ss_button"><i class="icon-resize-full"></i> <i class="icon-resize-small"></i></a></li> -->
                        <li><a href="#" id="ss_nextslide_button" class="ss_button"><i class="icon-chevron-right"></i></a></li>
                        <li><a href="#" id="ss_prevslide_button" class="ss_button"><i class="icon-chevron-left"></i></a></li>
                        
                    </ul>
                    <div class="clearfix"></div>
                

                </div><!-- /controls-->
		<?php get_template_part( "beforeloop", "fullwidth" ) ?> 
                
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            	<?php if ( post_password_required() ) : ?>
                                <div class="password_p">
                                    <?php the_content(); ?>
                                </div><!-- /password_p -->
            
                                <?php else : ?>
                        
                                
                                    <article id="post-<?php the_ID(); ?>" class="post">
                                    
											
                                        
                                            <?php 


                                            //Remove the original Gallery Shortcode from the content
                                        	function remove_gallery($content) {
												$patron = '/\[(\[?)(gallery)(?![\w-])([^\]\/]*(?:\/(?!\])[^\]\/]*)*?)(?:(\/)\]|\](?:([^\[]*+(?:\[(?!\/\2\])[^\[]*+)*+)\[\/\2\])?)(\]?)/';
												return preg_replace($patron, '', $content);
											}
											add_filter( 'the_content', 'remove_gallery'); 
											//------------------------------------------------------<

                                            the_content(); 
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            		
				$slider_autoplay = of_get_option('slider_autoplay'); 
				$slider_slide_interval = of_get_option('slider_slide_interval'); 
				$slider_speed_transition = of_get_option('slider_speed_transition'); 
				$slider_transition = of_get_option('slider_transition'); 
				$slider_pausehover = of_get_option('slider_pausehover_fullscreen'); 
				$slider_navigation = of_get_option('slider_navigation');
				$slider_verticalcenter = of_get_option('slider_verticalcenter');
				$slider_horizontalcenter = of_get_option('slider_horizontalcenter'); 
				
				?>

		        
		           	<!-- Supersized -->
		            <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/supersized.css" />
		            
		            <script type="text/javascript">
		            jQuery(document).ready(function($) { 
							
		                    
		                    supersized_fstart = function(){
		                    jQuery(function($){
		                    		
		                            $.supersized({
		                                //Functionality
                                slideshow               :   1,		//Slideshow on/off
                                autoplay				:	<?php echo $slider_autoplay; ?>,		//Slideshow starts playing automatically
                                start_slide             :   1,		//Start slide (0 is random)
                                random					: 	0,		//Randomize slide order (Ignores start slide)
                                slide_interval          :   <?php echo $slider_slide_interval; ?>,	//Length between transitions
                                transition              :   <?php echo $slider_transition; ?>, 		//0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
                                transition_speed		:	<?php echo $slider_speed_transition; ?>,	//Speed of transition
                                new_window				:	0,		//Image links open in new window/tab
                                pause_hover             :   <?php echo $slider_pausehover; ?>,		//Pause slideshow on hover
                                keyboard_nav            :   1,		//Keyboard navigation on/off
                                performance				:	1,		//0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
                                image_protect			:	1,		//Disables image dragging and right click with Javascript
                                image_path				:	'<?php echo get_template_directory_uri(); ?>/images/', //Default image path
            
                                //Size & Position
                                min_width		        :   0,		//Min width allowed (in pixels)
                                min_height		        :   0,		//Min height allowed (in pixels)
                                vertical_center         :   <?php echo $slider_verticalcenter; ?>,		//Vertically center background
                                horizontal_center       :   <?php echo $slider_horizontalcenter; ?>,		//Horizontally center background
                                fit_portrait         	:   1,		//Portrait images will not exceed browser height
                                fit_landscape			:   0,		//Landscape images will not exceed browser width
                                
                                //Components
                                navigation              :   <?php echo $slider_navigation; ?>,		//Slideshow controls on/off
                                thumbnail_navigation    :   0,		//Thumbnail navigation
                                slide_counter           :   0,		//Display slide numbers
                                slide_captions          :   0,		//Slide caption (Pull from "title" in slides array)
                                slides					:   [ 		//Background image
		                                                                    <?php 
																			if($wp_version >= 3.5){

																				$post_content = get_the_content();
																				preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
																				$array_id = explode(",", $ids[1]);

																				if (count($array_id) > 1) {
																				
																					$output = "";
																					$n= 0;



																						foreach ($array_id as $image_id) {
																							$meta_info = wp_get_attachment_metadata($image_id);
																							$image_attributes = wp_get_attachment_image_src( $image_id );
																								
																								
																								
																							$n++;
																							//Slide Image
																							$thumbnail = wp_get_attachment_url($image_id);
																						   
																						
																							$output.="{"; 
																							$output.="image : '". $thumbnail ."'"; 
																							$output.='  , caption : "'.esc_html(get_post($image_id)->post_excerpt).'"';
																							
																							if(get_post($image_id)->post_excerpt){
																								$output.="  , show_caption : 1";
																							}else{
																								$output.="  , show_caption : 0";																	
																							}		
																							$output.="},";

	
																							
																						}//foreach	
																						if($n > 1){
																						  $output = substr ($output, 0, -1);
																						}
																						 echo $output;
																				}//if count











																			}else{


																				$args = array(
																					'order'          => 'ASC',
																					'post_type'      => 'attachment',
																					'post_parent'    => $post->ID,
																					'post_mime_type' => 'image',
																					'post_status'    => null,
																					'numberposts'    => -1,
																				);
																				$attachments = get_posts($args);
																				if (count($attachments) > 1) {
																				
																					$output = "";
																					$n= 0;
																						foreach ($attachments as $attachment) {
																							$meta_info = wp_get_attachment_metadata($attachment->ID);
																								
																								
																								
																							$n++;
																							//Slide Image
																							$thumbnail = wp_get_attachment_url($attachment->ID);
																						   
																						
																							$output.="{"; 
																							$output.="image : '". $thumbnail ."'"; 
																							$output.="},";
	
																							
																						}//foreach	
																						if($n > 1){
																						  $output = substr ($output, 0, -1);
																						}
																						 echo $output;
																				}//if count

																			}// if wp_version											
																			?>
		                                                            ]
		                                        
		                            });//supersized
		                            
									//jQuery("#supersized").prepend('<div class="full_pattern" />');	
									
									
								});//jQuery(function($)
							}//supersized_fstart
							
							
							supersized_fstart();
							
		                  
		            });            
		            </script>
                                                  
                                        
                                            <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
                                            
                                            <div class="clear"></div>
                                           
                                     </article>
                                     
                            	<?php endif; //password ?>
                                
                        
                            <?php endwhile; else: ?>
                        
                                
                                    <article>
                                        <p>Sorry, no posts matched your criteria.</p>
                                    </article>
                               
                        
                            <?php endif; ?>
                    
                          <?php get_template_part( "afterloop", "fullwidth" ) ?> 

<?php get_footer(); ?>