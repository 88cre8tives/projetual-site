<?php
function get_defaults()
{
	$default = array(
		'type'		   => 'jquery',
		'width'		   => 600,
		'height'		  => 400,
		'auto'       	   => 'on',
		'delay'       	   => 5000,
		'effect'		=> 'on',
		'handelSpeed'      => 0,	
		'handelColor'	=> '#000000',
		'handelShadow'	=> 'on',		
		'borderWidth'     => 0,
		'borderColor'     => '#ffffff',
		'controlDisplay'  => 'on',
		'paginationDisplay'  => 'on',
		'cpationDisplay'  => 'on',
		'captionSize'     => 13,
		'captionColor'    => '#ffffff',
		'captionAlignment'=> 'center',
		'captionBG'		=> '#222222',
		'bacpationDisplay'	=> 'on',
	);
	return $default;
}
function default_set()
{
	$set = array();
	$set[0] = array(
		"before" => plugins_url().'/baslider/images/set-1-before.jpg',
		"after" => plugins_url().'/baslider/images/set-1-after.jpg',
		"caption" => 'Image Set 1',
	);
	$set[1] = array(
		"before" => plugins_url().'/baslider/images/set-2-before.jpg',
		"after" => plugins_url().'/baslider/images/set-2-after.jpg',
		"caption" => 'Image Set 2',
	);
	return $set;
}
?>