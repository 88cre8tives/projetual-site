/**
 * BASlider - Multipurpose Before After Slider
 * Copyright 2013, Brainstorm Force, http://brainstormforce.com/
*/
var $mbas11 = jQuery.noConflict();
$mbas11(document).ready(function ($mbas11) {
	// Before image upload
	var _custom_media = true,
		_orig_send_attachment = wp.media.editor.send.attachment;
	$mbas11('.thumb-before').click(function (e) {
		var send_attachment_bkp = wp.media.editor.send.attachment;
		var button = $mbas11(this);
		var id = button.attr('id').replace('_button', '');
		_custom_media = true;
		wp.media.editor.send.attachment = function (props, attachment) {
			if (_custom_media) {
				$mbas11("#" + id).val(attachment.id);
				$mbas11('#img-thumb-before').html("<img src='" + attachment.url + "'>");
			} else {
				return _orig_send_attachment.apply(this, [props, attachment]);
			};
		}
		wp.media.editor.open(button);
		return false;
	});
	// After image upload
	$mbas11('.thumb-after').click(function (e) {
		var send_attachment_bkp = wp.media.editor.send.attachment;
		var button = $mbas11(this);
		var id = button.attr('id').replace('_button', '');
		_custom_media = true;
		wp.media.editor.send.attachment = function (props, attachment) {
			if (_custom_media) {
				$mbas11("#" + id).val(attachment.id);
				$mbas11('#img-thumb-after').html("<img src='" + attachment.url + "'>");
			} else {
				return _orig_send_attachment.apply(this, [props, attachment]);
			};
		}
		wp.media.editor.open(button);
		return false;
	});
	$mbas11('.add_media').on('click', function () {
		_custom_media = false;
	});
	// Edit Before image upload
	var _custom_media = true,
		_orig_send_attachment = wp.media.editor.send.attachment;
	$mbas11('.edit-before').click(function (e) {
		var send_attachment_bkp = wp.media.editor.send.attachment;
		var button = $mbas11(this);
		var id = button.attr('id').replace('_button', '');
		_custom_media = true;
		wp.media.editor.send.attachment = function (props, attachment) {
			if (_custom_media) {
				$mbas11("#" + id).val(attachment.id);
				$mbas11('#edit-thumb-before').html("<img src='" + attachment.url + "'>");
			} else {
				return _orig_send_attachment.apply(this, [props, attachment]);
			};
		}
		wp.media.editor.open(button);
		return false;
	});
	// Edit After image upload
	$mbas11('.edit-after').click(function (e) {
		var send_attachment_bkp = wp.media.editor.send.attachment;
		var button = $mbas11(this);
		var id = button.attr('id').replace('_button', '');
		_custom_media = true;
		wp.media.editor.send.attachment = function (props, attachment) {
			if (_custom_media) {
				$mbas11("#" + id).val(attachment.id);
				$mbas11('#edit-thumb-after').html("<img src='" + attachment.url + "'>");
			} else {
				return _orig_send_attachment.apply(this, [props, attachment]);
			};
		}
		wp.media.editor.open(button);
		return false;
	});
	$mbas11('.add_media').on('click', function () {
		_custom_media = false;
	});
});