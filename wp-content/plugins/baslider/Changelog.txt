Changelog:
October 17, 2014 - Version 2.4.2
- Improvement: Before/After custom text
Augest 08, 2014 - Version 2.4.1
- Fixed: Backend issue of visual composer conflict
December 18, 2013 - Version 2.3
- Fixed: 'get_width' not defined issue with slider script.
December 13, 2013 - Version 2.2
- Improvement: Compatibility with WordPress 3.8
- Improvement: Compatibility with Visual Composer
- Improvement: Plugin JS and CSS now loads only on pages where slider is loaded
- Fixed: CSS conflicts on add image set screen
November 20, 2013 � Version 2.1
- Improvement: Documentation
- Improvement: Default Settings
- New: Added Component for Visual Composer
October 19, 2013 � Version 2.0
- Front-end design of the slider is revamped completely
- More settings for the admin to control display of the front-end
- Design changes in the admin settings panel