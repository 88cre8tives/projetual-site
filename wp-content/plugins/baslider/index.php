<?php
/*
Plugin Name: MultiPurpose Before After Slider
Plugin URI: https://www.brainstormforce.com
Author: Brainstorm Force
Author URI: https://www.brainstormforce.com
Description: Compare images with ease!
Version: 2.4.2
Text Domain: baslider
*/
//define('CONCATENATE_SCRIPTS', false );
if(!class_exists('ba_slider'))
{
	class ba_slider
	{
		function __construct()
		{
			// Runs when plugin is activated and creates new database field
			register_activation_hook(__FILE__, array( $this, 'baslider_defaults' ) );
			// Including JS & CSS for frontend
			add_action('wp_enqueue_scripts', array( $this, 'baslider_head_scripts' ) );
			add_action('wp_enqueue_scripts', array( $this, 'baslider_head_styles' ) );
			// Include styles & scripts for dashboard
			//add_action('admin_enqueue_scripts', array( $this, 'baslider_admin_scripts' ) );
			add_action('admin_enqueue_scripts', array( $this, 'baslider_admin_styles' ) );
			// Including Custom Scripts
			add_action('admin_menu', array( $this, 'baslider_plugin_admin_menu' ) );
			$plugin = plugin_basename(__FILE__); 
			add_filter("plugin_action_links_$plugin", array( $this,'bsf_settings_link') );
			add_action('wp_head', array($this, 'custom_script'));
			// Register new_slider action for ajax call
			add_action( 'wp_ajax_new_slider', array( $this, 'new_slider' ));	
			add_action( 'wp_ajax_nopriv_new_slider', array($this, 'new_slider') );		
			// Register new_set action for ajax call
			add_action( 'wp_ajax_new_set', array( $this, 'add_slider_set' ));
			// Register update_setting action for ajax call
			add_action( 'wp_ajax_update_setting', array( $this, 'update_setting' ));
			// Register reset_setting action for ajax call
			add_action( 'wp_ajax_reset_setting', array($this, 'reset_settings' ));			
			// Register edit _set action for ajax call
			add_action( 'wp_ajax_edit_set', array($this, 'edit_set' ));
			// Register del_set action for ajax call
			add_action( 'wp_ajax_del_set', array($this, 'del_set' ));
			// Add select box near media button for adding shortcode
			add_action('media_buttons',array( $this, 'add_ba_select'),11);
			add_action('admin_head',array( $this,'button_js'));
			add_action('wp_head',array($this,'scripts_footer'));
			//Add shortcode
			add_shortcode('baslider',array($this, 'baslider_short_code'));
			// Add visual composer component
			add_action('init',array($this,'baslider_vc'));
			add_action('admin_init', array($this, 'baslider_redirect'));							
		}
		function bas_get_defaults(){
			$default = array(
				'type'		   => 'jquery',
				'width'		   => 600,
				'height'		  => 400,
				'auto'       	   => 'on',
				'delay'       	   => 5000,
				'effect'		=> 'on',
				'handelSpeed'      => 0,	
				'handelColor'	=> '#000000',
				'handelShadow'	=> 'on',		
				'borderWidth'     => 0,
				'borderColor'     => '#ffffff',
				'controlDisplay'  => 'on',
				'paginationDisplay'  => 'on',
				'cpationDisplay'  => 'on',
				'captionSize'     => 13,
				'captionColor'    => '#ffffff',
				'captionAlignment'=> 'center',
				'captionBG'		=> '#222222',
				'bacpationDisplay'	=> 'on',
			);
			return $default;
		}
		function bas_default_set(){
			$set = array();
			$set[0] = array(
				"before" => plugins_url().'/baslider/images/set-1-before.jpg',
				"after" => plugins_url().'/baslider/images/set-1-after.jpg',
				"caption" => 'Image Set 1',
			);
			$set[1] = array(
			"before" => plugins_url().'/baslider/images/set-2-before.jpg',
			"after" => plugins_url().'/baslider/images/set-2-after.jpg',
			"caption" => 'Image Set 2',
			);
			return $set;
		}	
		function baslider_redirect()
		{
			if(get_option('baslider_activate'))
			{
				$theme_name = get_current_theme();
				if($theme_name !== "smile")
				{
					delete_option('baslider_activate');
					if ( is_multisite() ) {
						$blog_id = get_current_blog_id();
						$url = get_admin_url( $blog_id ).'admin.php?page=baslider'; 
						wp_redirect($url);
						exit;
					} else {
						$url = admin_url().'admin.php?page=baslider'; 
						wp_redirect($url);
						exit;
					}
				}			
			}
		}
		// Add balider short code  use baslider as  [baslider name='baslider_options']
		function baslider_short_code($atts) {
			ob_start();
			extract(shortcode_atts(array(
				"name" => ''
			), $atts));
			display_baslider($name);
			$output = ob_get_clean();
			return $output;
		}
		function baslider_vc()
		{
			/*Add Before After Slider Component*/
			$sliders = get_option('baslider');
			if ( function_exists('vc_map'))
			{
				vc_map( array(
						"name" => __("Before After", 'baslider'),
						"base" => "baslider",
						"class" => "before_after",
						"controls" => "full",
						"show_settings_on_create" => true,
						"icon" => "icon-before-after",
						"category" => __('Content', 'baslider'),
						"params" => array(
							array(
								"type" => "dropdown",
								"class" => "",
								"heading" => __("Slider", "baslider"),
								"param_name" => "name",
								"admin_label" => true,
								"value" => $sliders,
								"description" => __("Select the Before After Slider you want to display.","baslider")
							),
						)// End params array
					) // End vc_map array
				); // End vc_map
			} // End if function_exists
		}
		function scripts_footer()
		{
			$uni = uniqid();
				echo '<script type="text/javascript">
					var $mbas2 = jQuery.noConflict();
					function get_width(id)
					{
						var width = $mbas2(id).width();
						var half_div = width/2;
						return half_div;
					}
					function start_slider(id,delay,auto,effect,left,right,pager)
					{
						var slider'.$uni.' = $mbas2(".slides-"+id).bxSlider({
							auto: auto,
							pause: delay,
							pager: pager,
							nextSelector: "#slider-next-"+id,
							prevSelector: "#slider-prev-"+id,
							mode:effect,
							adaptiveHeight: true,
							onSlideAfter: function(){
								reset_images(id);
								$mbas2(window).on("resize", function(event){
									reset_images(id);
								});
							},
							onSliderLoad: function(){
								//console.log("Loaded");
							}
						});
						/*
						for custom popup or tabs 
						$mbas4(document).on("click",".btn-lg",function (e) {
							setTimeout(function(){
								slider'.$uni.'.redrawSlider();
							},1000);
						});*/
					}
					
					function reset_images(id)
					{
						var width = get_width("#"+id);
						var left = width;
						var right = width+2;
						$mbas2("#"+id+" .topImage").css("left",left);
						$mbas2("#"+id+" .topImg").css("left",-right);
						check_for_traditional();
					}
					function check_for_traditional()
					  {
						$mbas1(".beforeAfterSlidebar").each(function(index,value){
							if($mbas1(this).hasClass("traditional_slider"))
								$mbas1(this).find(".topImg").css("left","0px");	
						});  
					  }
					
				</script>';
		}
		// Generate select box of shortcodes
		function add_ba_select()
		{
			$sliders = get_option('baslider');
			echo '&nbsp;<select id="ba_select" style="padding: 3px; margin: 0; border-radius: 4px; color: #222; outline: none; display: inline-block; border: 1px solid #999;">
				<option disabled="disabled" selected="selected">Before After Slider</option>';
			foreach($sliders as $slider)
			{?>
				<option value='[baslider name="<?php echo $slider; ?>"]'><?php echo $slider; ?></option>
			<?php }
			echo '</select>';
		}
		// Register jquery for button function
		function button_js() 
		{
			echo '<script type="text/javascript">
			(function( $ ) {
    				$(document).ready(function(){
			   		$("#ba_select").change(function() {
					  send_to_editor($("#ba_select :selected").val());
						  return false;
					});
				});
			})( jQuery );
			</script>';
		}
		// Function to handle ajax call for reset setting
		function reset_settings()
		{
			if(isset($_POST['slider_name']))
			{
				$slider_name = $_POST['slider_name'];
				delete_option($slider_name.'_setting');
				//require_once('config.php');
				$default = $this->bas_get_defaults();
				echo add_option($slider_name.'_setting', $default) ? 'restored' : 'error';
			}
			die();
		}
		// Function to handle ajax call for update setting
		function update_setting()
		{
			$slider_name = $_POST['slider_name'];
			$slider_settings = array(
					'type'		      => $_POST['type'],
					'width'		     => $_POST['width'],
					'height'		    => $_POST['height'],
					'delay'       	     => $_POST['delay'],
					'handelSpeed'       => $_POST['handelSpeed'],
					'handelColor'       => $_POST['handelColor'],
					'handelShadow'      => isset($_POST['handelShadow']) ? $_POST['handelShadow'] : '',
					'auto'       	      => isset($_POST['auto']) ? $_POST['auto'] : '', 
					'effect'       	    => isset($_POST['effect']) ? $_POST['effect'] : '', 
					'borderWidth'       => $_POST['borderWidth'],
					'borderColor'       => $_POST['borderColor'],
					'controlDisplay'    => isset($_POST['controlDisplay']) ? $_POST['controlDisplay'] : '',
					'paginationDisplay' => isset($_POST['paginationDisplay']) ? $_POST['paginationDisplay'] : '',
					'cpationDisplay'    => isset($_POST['cpationDisplay']) ? $_POST['cpationDisplay'] : '',
					'captionSize'       => $_POST['captionSize'],
					'captionColor'      => $_POST['captionColor'],
					'captionAlignment'  => $_POST['captionAlignment'],
					'captionBG'		 => $_POST['captionBG'],
					'bacpationDisplay'  => isset($_POST['bacpationDisplay']) ? $_POST['bacpationDisplay'] : '',
					
				);
			echo update_option($slider_name.'_setting',$slider_settings) ? 'updated' : 'error';
			die();
		}
		// Function to handle ajax call for adding new slider
		function new_slider()
		{
			// Add new slider
			if(isset($_POST['slider_name']) && trim($_POST['slider_name']) != "")
			{
				$slider_name = $_POST['slider_name'];
				$sliders = get_option('baslider');
				$slider_name = preg_replace('/[^a-z0-9\s]/i', '', $slider_name);  
				$slider_name = str_replace(" ", "_", $slider_name);
				if(!in_array($slider_name,$sliders))
				{
					$slider_setting = $slider_name.'_setting';
					//require_once('config.php');
					$default = $this->bas_get_defaults();
					add_option($slider_setting, $default);
					array_push($sliders,$slider_name);
					if(update_option('baslider',$sliders))
						echo 'added';
					else
						echo 'err';
				} else{
					echo 'name';
				}
			} else {
				//echo 'empty';
			}
			die();
		}
		// Add new slider set
		function export_slider(){			
			if(isset($_POST['export']))
			{
				$slider_name = $_POST['export'];
				$output ='_<single>';
				$settings = get_option($slider_name.'_setting');
				$data = get_option($slider_name);				
				$export = array('settings'=>$settings,'slider_name'=>$slider_name,'slider_data'=>$data);				
				$output.= json_encode($export);				
				$file_url = plugin_dir_path(__FILE__).'export/';		
				$file = $file_url.$slider_name.'.baslider';				
				if(file_put_contents ( $file , $output)){					
					echo 'exported';//plugins_url().'/'.$slider_name.'.basexp';					
				}				
			}			
			if(isset($_POST['export_all']))
			{
				$baslider = get_option('baslider');
				$loc = $output = '';
				$output ="_<multi>";				
				foreach ($baslider as $key => $value) {
					$output.="<slider>";
					$slider_name = $value;
					$settings = get_option($slider_name.'_setting');
					$data = get_option($slider_name);				
					$export = array('settings'=>$settings,'slider_name'=>$slider_name,'slider_data'=>$data);				
					$output.= json_encode($export);					
					$file_url = plugin_dir_path(__FILE__).'export/';		
					$file = $file_url.get_bloginfo('name').'_all.baslider';					
				}				
				if(file_put_contents ( $file , $output)){					
					echo 'all_exported';//plugins_url().'/'.$slider_name.'.basexp';
				}	
			}
			die();
		}
		function add_slider_set()
		{
			// Upload a new set
			if(isset($_POST['upload_set']) && trim($_POST['before']) != "" && trim($_POST['after']) != "" && trim($_POST['caption']) != "" && trim($_POST['slider_name']) != "" )
			{
				$caption_before = (isset($_POST['caption_before']) && $_POST['caption_before'] != '') ? $_POST['caption_before'] : 'Before';
				$caption_after = (isset($_POST['caption_after']) && $_POST['caption_after'] != '') ? $_POST['caption_after'] : 'After';
				
				$slider_name = $_POST['slider_name'];
				$set_info = array(
					'before'		   => $_POST['before'],
					'after'		   => $_POST['after'],
					'caption'		   => $_POST['caption'],
					'caption_before' => $caption_before,
					'caption_after' => $caption_after
				);
				$slider_set = array();
				$slider_set[] = $set_info;
				$prev_slider_set = get_option($slider_name);
				$sets = count($prev_slider_set);
				if($prev_slider_set == "")
					$result = add_option($slider_name,$slider_set);
				else
				{
					//$c = count($prev_slider_set)+1;
					//$new_set = array();
					//$new_set['set_'.$c] = $set_info;
					//$prev_slider_set = $prev_slider_set + $new_set;
					array_push($prev_slider_set, $set_info);					
					$result = update_option($slider_name,$prev_slider_set);
				}
				if($result)
				{
					echo 'success';
				}
			} else {
				echo 'err';
			}
			die();
		}
		function edit_set()
		{
			// Update existing set with new values
			$slider_name = $_POST['slider_name'];
			$caption_before = (isset($_POST['caption_before']) && $_POST['caption_before'] != '') ? $_POST['caption_before'] : 'Before';
			$caption_after = (isset($_POST['caption_after']) && $_POST['caption_after'] != '') ? $_POST['caption_after'] : 'After';
			if(isset($_POST['update_set']))
			{
				$set = $_POST['set'];
				$set_info = array(
					'before'		   => $_POST['before'],
					'after'		   => $_POST['after'],
					'caption'		   => $_POST['caption'],
					'caption_before' => $caption_before,
					'caption_after' => $caption_after
				);
				$prev_sets = get_option($slider_name);
				foreach($prev_sets as $key => $p_set)
				{
					if($key == $set)
					{
						$prev_sets[$key]['before'] = $_POST['before'];
						$prev_sets[$key]['after'] = $_POST['after'];
						$prev_sets[$key]['caption'] = $_POST['caption'];
						$prev_sets[$key]['caption_before'] = $caption_before;
						$prev_sets[$key]['caption_after'] = $caption_after;
					}
				}
				$result = update_option($slider_name,$prev_sets);
				if($result)
				{
					return $result;
				}
			}
			die();
		}
		function del_set()
		{
			// Delete a set
			if(isset($_POST['set']))
			{
				$set = $_POST['set'];
				$slider_name = $_POST['slider'];
				$prev_sets = get_option($slider_name);
				foreach($prev_sets as $key => $p_set)
				{
					if($key == $set)
					{
						unset($prev_sets[$key]);
					}
				}
				$result = update_option($slider_name,$prev_sets);
				if($result)
				{
					return $result;
				}
			}
			die();
		}
		function baslider_head_scripts() 
		{
			wp_register_script('baslider', plugins_url('js/jquery.baslider.min.js', __FILE__));	
			wp_enqueue_script('jquery');
			wp_enqueue_script('baslider');
		}
		function baslider_head_styles() 
		{
			wp_register_style('baslider_main_style', plugins_url('css/style.css', __FILE__));
			wp_enqueue_style('baslider_main_style');
		}
		function baslider_admin_scripts() 
		{
			wp_enqueue_script('jquery');
			wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
			wp_enqueue_media();
			wp_register_script('new-media', plugins_url('js/jquery.media.upload.js', __FILE__));	
			wp_enqueue_script('new-media');
		}
		function baslider_admin_styles() 
		{
			wp_register_script('jqform', plugins_url('js/jquery.form.js', __FILE__));	
			wp_enqueue_script('jqform');
			wp_register_style('baslider_style', plugins_url('css/admin.css', __FILE__));
			wp_enqueue_style('baslider_style');
			wp_enqueue_style('thickbox');
		}
		function baslider_defaults() 
		{
			global $wp_version;
			$wp = 3.5;
			if( version_compare( $wp_version, $wp, '<' ) )
			{
				deactivate_plugins( basename( __FILE__ ) );
				wp_die('<p>The <strong>MultiPurpose Before After Slider </strong> plugin requires WordPress version 3.5 or greater.</p>','Plugin Activation Error',  array( 'response'=>200, 'back_link'=>TRUE ) );
			}
			else
			{
				if(get_option('baslider')){
					$prev_sets= get_option('baslider');
					foreach ($prev_sets as $value) {
						$new_indexed_sets = array();
						$sets = get_option($value);
						foreach ($sets as $key => $values) {
							array_push($new_indexed_sets,$values);
						}
						delete_option($value);					
						update_option($value,$new_indexed_sets);
					}
				}
				$sliders = array('slider1');
				if(!get_option('baslider'))
				{
					//require_once('config.php');
					add_option('baslider',$sliders);
					$default = $this->bas_get_defaults();
					add_option('slider1_setting', $default);
					$default_set = $this->bas_default_set();
					add_option('slider1', $default_set);
				}
				add_option('baslider_activate',1);
				update_option('bas_tour_status','on');
			}
		}
		function baslider_plugin_admin_menu() 
		{
			require_once('add_slider.php');
			add_menu_page('Add BASlider', 'Before After', 'administrator', 'baslider', 'baslider_main', plugins_url( 'baslider/icon-16.png' ));
			require_once('slider_settings.php');
			$page1 = add_submenu_page('baslider','','', 'publish_posts', 'add-baslider', 'baslider_admin_page');
			add_action('admin_print_scripts-' . $page1, array( $this, 'baslider_admin_scripts' ) );
			require_once('add_image_set.php');
			$page = add_submenu_page('baslider','','', 'publish_posts', 'baslider-settings', 'baslider_setting_page');
			add_action('admin_print_scripts-' . $page, array( $this, 'iris_enqueue_scripts' ) );			
		}
		function iris_enqueue_scripts() 
		{
				wp_enqueue_script( 'wp-color-picker' );
				// load the minified version of custom script
				wp_enqueue_script( 'cp_custom', plugins_url( 'js/cp-script.min.js', __FILE__ ), array( 'jquery', 'wp-color-picker' ), '1.1', true );
				wp_enqueue_style( 'wp-color-picker' );
		}
		//add_action( 'admin_enqueue_scripts', 'iris_enqueue_scripts' );
		// Add settings link on plugin page
		function bsf_settings_link($links) 
		{ 
		  $settings_link = '<a href="admin.php?page=baslider">Settings</a>'; 
		  array_unshift($links, $settings_link); 
		  return $links; 
		}
		// Function custom script	
		function custom_script()
		{
		?>
				<script type="text/javascript">
                var $mbas1 = jQuery.noConflict();
                $mbas1(document).ready(function() {
                  $mbas1(".beforeAfterSlidebar").mousemove(
                    function(e) {	
                      // get the mouse x (horizontal) position and offset of the div
                      var offset =  $mbas1(this).offset();
                      var iTopLeft = (e.pageX - offset.left);
                      var iTopImgLeft = -(iTopLeft+2);
                      // set left of bottomimage div
                      if(!$mbas1(this).hasClass('traditional_slider'))
                      {
                        $mbas1(this).find(".topImage").css('left',iTopLeft);
                        $mbas1(this).find(".topImg").css('left',iTopImgLeft);
                      }
                      else
                        check_for_traditional();
                    }
                  );
                  function check_for_traditional()
                  {
                    $mbas1(".beforeAfterSlidebar").each(function(index,value){
                        if($mbas1(this).hasClass('traditional_slider'))
                            $mbas1(this).find(".topImg").css('left','0px');	
                    });  
                  }
                })
                </script> 
		<?php
        }       
	}
}
// Display Slider
function display_baslider($slider_name)
{
	$slider_array = get_option($slider_name);
	$ba_setting = get_option($slider_name.'_setting');
	
	$captionDisplay = $ba_setting['cpationDisplay'];
	
	$controlDisplay = $ba_setting['controlDisplay'];
	$paginationDisplay = $ba_setting['paginationDisplay'];
	$effect = $ba_setting['effect'];
	$auto = $ba_setting['auto'];
	$handelShadow = $ba_setting['handelShadow'];
	$bacpationDisplay = $ba_setting['bacpationDisplay'];
	
	$cptd = ($captionDisplay == 'on') ? '' : 'display:none !important;';
	$ctrld = ($controlDisplay == 'on') ? '' : 'display:none !important;';
	$pgntd = ($paginationDisplay == 'on') ? 'true' : 'false';
	//$efct = ($effect == 'on') ? 'fade' : 'horizontal';
	$efct = 'fade';
	$atply = ($auto == 'on') ? 'true' : 'false';
	$hndshdw = ($handelShadow == 'on') ? '' : 'box-shadow:none;';
	$bacptd = ($bacpationDisplay == 'on') ? '' : 'style="display: none !important;"';
	
	$baid = $slider_name.'-'.uniqid();
	if(!empty($slider_array))
	{
	?>
	
	<div id="<?php echo $baid;?>" class="baslider-main" style=" width: 600px; border: <?php echo $ba_setting['borderWidth'];?>px solid <?php echo $ba_setting['borderColor'];?>;">
		<ul class="slides-<?php echo $baid;?>">
		<?php
			foreach($slider_array as $slider)
			{
				$before_caption = (isset($slider['caption_before']) && $slider['caption_before'] != '') ? $slider['caption_before'] : 'Before';
				$after_caption = (isset($slider['caption_after']) && $slider['caption_after'] != '') ? $slider['caption_after'] : 'After';
				if(is_numeric($slider['before'])){
					$slider_before_img = wp_get_attachment_image_src($slider['before'],'full');
					$slider_before_img = $slider_before_img[0];
				}
				else
					$slider_before_img = $slider['before'];
					
				if(is_numeric($slider['after']))
				{
					$slider_after_img = wp_get_attachment_image_src($slider['after'],'full');
					$slider_after_img = $slider_after_img[0];
				}
				else
					$slider_after_img = $slider['after'];
				
			?>
			
			<?php
			if ($after_caption == '.') { ?>
				<li class="baslideli">
					<div class="beforeAfterSlidebar <?php if ($ba_setting['type'] == 'traditional') echo 'traditional_slider'; ?>">
						<div class="bottomImage" <?php if ($ba_setting['type'] == 'traditional') echo 'style="width:50% !important;"'; ?>>
							<img src="<?php echo $slider_before_img; ?>" alt="Before-<?php echo $slider['caption'];?>">
						</div>
	                    <div class="topImage_nobar" style="border-left-color:<?php echo $ba_setting['handelColor']; if ($ba_setting['type'] == 'traditional') echo '; width:50% !important; left:50% !important;'; ?>;-webkit-transition: <?php echo $ba_setting['handelSpeed'];?>ms;-moz-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;-o-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;-ms-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms; <?php echo $hndshdw;?>">
							<img class="topImg" style="-webkit-transition: <?php echo $ba_setting['handelSpeed'];?>ms;-moz-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;-o-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;-ms-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;" src="<?php echo $slider_after_img; ?>" alt="After-<?php echo $slider['caption']?>">
						</div><h5 style="text-align:<?php echo $ba_setting['captionAlignment'];?>;color:<?php echo $ba_setting['captionColor']?> !important;"><span class="ba-before" <?php echo $bacptd;?>><?php _e($before_caption,'baslider');?></span><span class="ba-title" style="font-size:<?php echo $ba_setting['captionSize'];?>px; <?php echo $cptd;?>"><?php echo $slider['caption']?></span><span class="ba-after" <?php echo $bacptd;?>><?php _e($after_caption,'baslider');?></span></h5></div>
				</li>
			
			<?php
			}
			else { ?>
				<li class="baslideli">
				<div class="beforeAfterSlidebar <?php if ($ba_setting['type'] == 'traditional') echo 'traditional_slider'; ?>">
					<div class="bottomImage" <?php if ($ba_setting['type'] == 'traditional') echo 'style="width:50% !important;"'; ?>>
						<img src="<?php echo $slider_before_img; ?>" alt="Before-<?php echo $slider['caption'];?>">
					</div>
                    <div class="topImage" style="border-left-color:<?php echo $ba_setting['handelColor']; if ($ba_setting['type'] == 'traditional') echo '; width:50% !important; left:50% !important;'; ?>;-webkit-transition: <?php echo $ba_setting['handelSpeed'];?>ms;-moz-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;-o-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;-ms-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms; <?php echo $hndshdw;?>">
						<img class="topImg" style="-webkit-transition: <?php echo $ba_setting['handelSpeed'];?>ms;-moz-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;-o-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;-ms-transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;transition-duration: <?php echo $ba_setting['handelSpeed'];?>ms;" src="<?php echo $slider_after_img; ?>" alt="After-<?php echo $slider['caption']?>">
					</div><h5 style="text-align:<?php echo $ba_setting['captionAlignment'];?>;color:<?php echo $ba_setting['captionColor']?> !important;"><span class="ba-before" <?php echo $bacptd;?>><?php _e($before_caption,'baslider');?></span><span class="ba-title" style="font-size:<?php echo $ba_setting['captionSize'];?>px; <?php echo $cptd;?>"><?php echo $slider['caption']?></span><span class="ba-after" <?php echo $bacptd;?>><?php _e($after_caption,'baslider');?></span></h5></div>
				</li>
			<?php
			}
			?>
			
			
			
			
			
			<?php
			} ?>
		</ul>
	</div>
	<div class="ba-outside" style="width:<?php echo ($ba_setting['width']+($ba_setting['borderWidth']*2));?>px;<?php echo $ctrld;?>">
		<div style="left: 80%; top: -40px; position: absolute;"><span id="slider-next-<?php echo $baid;?>" class="slide-nex"></span>&nbsp;&nbsp;<span id="slider-prev-<?php echo $baid;?>" class="slide-pre"></span></div>
	</div>
	<div style="clear:both"></div>
		<script type="text/javascript">
		var $mbas4 = jQuery.noConflict();
                $mbas4(document).ready(function(){
                        var <?php echo $slider_name ?>_l = get_width('#<?php echo $baid;?>');
                        var <?php echo $slider_name ?>_r = get_width('#<?php echo $baid;?>');
                        var <?php echo $slider_name ?>_l = <?php echo $slider_name ?>_l-2;
                        var <?php echo $slider_name ?>_r = <?php echo $slider_name ?>_r;
                        var <?php echo $slider_name ?>_auto = <?php echo $atply;?>;
                        var <?php echo $slider_name ?>_delay = '<?php echo $ba_setting['delay'] ?>';
                        var <?php echo $slider_name ?>_effect = '<?php echo $efct;?>';
						var <?php echo $slider_name ?>_pager = <?php echo $pgntd;?>;
                    	reset_images('<?php echo $baid ?>');
                        start_slider('<?php echo $baid ?>',<?php echo $slider_name ?>_delay, <?php echo $slider_name ?>_auto, <?php echo $slider_name ?>_effect, <?php echo $slider_name ?>_l, <?php echo $slider_name ?>_r, <?php echo $slider_name ?>_pager);
                });
                $mbas4(window).on("resize", function(event){
                    reset_images('<?php echo $baid ?>');
                });
        </script>
	<?php
	}
}
// Instantiat the class
if(class_exists('ba_slider'))
{
	$ba_slider = new ba_slider();
}
// Register Widget for Before After Slider
require_once('functions.widget.php');
?>