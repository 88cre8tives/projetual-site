<?php
// Add BASlider
ob_start();
// Add slider page
function baslider_main()
{?>
    <div class="wrap bas-admin-wrap bas-add-slider-page">
    	<div class="baslider-heading">
	    	<div id="icon-baslider" class="icon32"></div>
			<h2><?php _e("Before After Slider", 'baslider'); ?></h2>
		</div>
        <span class="msg"></span>
	<?php
	//BASlider Functions
	if(isset($_POST['delete']))
	{
		$slider_name = $_POST['delete'];
		delete_option($slider_name.'_setting');
		delete_option($slider_name);
		$sliders = get_option('baslider');
		if(in_array($slider_name, $sliders))
		{
			if (($key = array_search($slider_name, $sliders)) !== false) {
				unset($sliders[$key]);
			}			
			if(update_option('baslider', $sliders))
			{
				return true;
			}
		}
	}	
	?>
		<table class="widefat" cellspacing="0" style="margin-bottom:15px;border-bottom:0;">
            <tbody>
            <form method="post" id="newSlider" action="">
               <tr> 
               <td style="width:70%;padding: 12px;">
                <input type="text" id="slider_name" name="slider_name" pattern="[a-zA-Z]*" onkeypress="return checkAlphaNumeric(event);" style="width: 100%; height: 34px;" placeholder="Enter Slider Name" />
               </td>
             </form>
               <td style="width:30%;text-align:right;padding: 10px; vertical-align: middle;">
               		<span class="loading"></span>
                    <button class="new_slider button-primary button-large">Create new BASlider</button>                    
               </td>
               </tr>
             </tbody>
		</table>
        <hr style="background: #DFDFDF;height: 1px;border: 0;" />
        <table class="widefat" cellspacing="0" style="margin-top:15px;">
            <tbody>
			<?php
            $sliders = get_option('baslider');
            foreach($sliders as $key => $slider)
            {?>
               <tr class="slider-<?php echo $key; ?> <?php echo $slider; ?>" style="height:40px;">
               <td class="row-title" style="width: 40%;text-align:left;vertical-align:middle;padding: 7px 7px 7px 20px;"><a href="?page=add-baslider&edit=<?php echo $slider;?>"><?php echo $slider;?></a></td>
               <td style="width: 30%;text-align:center;vertical-align: middle;padding: 7px;" >
					<a href="?page=baslider-settings&setting=<?php echo $slider;?>" class="ba-button-setting"></a>
               </td>
               <td style="width: 15%;text-align:center;vertical-align: middle;padding: 7px;" >
               		<a href="?page=add-baslider&edit=<?php echo $slider;?>" class="button ba-button">Edit</a>
               </td>
               <td style="width: 15%;text-align:center;vertical-align: middle;padding: 7px;" >
               		<button onclick="deleteSlider('<?php echo $slider; ?>');" class="button-primary ba-button <?php echo $slider;?>_del" > <?php _e('Delete','baslider'); ?></button>
               </td>
               </tr>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
    <script type="text/javascript">
	var $mbas7 = jQuery.noConflict();
	$mbas7(".new_slider").click(
	function(){
		newSlider();
	});
	// Create new Slider
	function newSlider()
	{
		$mbas7(".loading").html('<img src="<?php echo plugins_url().'/baslider/images/load.gif';?>" />');
		var slidername = $mbas7("#slider_name").val();
		var slider = $mbas7("#newSlider").serialize();
		var form_data = "action=new_slider&" + slider;
		$mbas7.post(ajaxurl, form_data, 
		function(status){
			console.log(status);
			if(status)
			{
				if(status == 'added')
					$mbas7(".msg").html('<div class="updated" id="message"><p><strong> Slider '+ slidername +' is successfully added ! </strong></p></div>');
				else if(status == 'err')
					$mbas7(".msg").html('<div class="error" id="message"><p><strong> Slider '+ slidername +' could not be added. </strong></p></div>');
				else if(status == 'name')
					$mbas7(".msg").html('<div class="error" id="message"><p><strong> Slider '+ slidername +' is already exists. Try another name. </strong></p></div>');
				else if(status == 'empty')
					$mbas7(".msg").html('<div class="error" id="message"><p><strong> Please provide a valid slider name. </strong></p></div>');
				$mbas7(".loading").html('');
				$mbas7("#message").fadeOut(3000);
				if(status == 'added')
					window.location='admin.php?page=add-baslider&edit='+slidername;
			}
		});
	}
	// Delete Slider
	function deleteSlider(slider)
	{
		$mbas7("."+slider+'_del').html('<img src="<?php echo plugins_url().'/baslider/images/load.gif';?>" />');
		var slider = slider;
		$mbas7.post('#',{'delete':slider},function(res){
			if(res)
			{
				$mbas7("."+slider).hide('slow');
				$mbas7(".msg").html('<div class="updated" id="message"><p><strong> Slider Deleted Successfully ! </strong></p></div>');
				$mbas7(".updated").fadeOut(3000);
			}				
		});
	}
	function checkAlphaNumeric(e) {
		if ((e.which >= 65 && e.which <= 90) ||
		   (e.which >= 97 && e.which <= 122))
			return true;
		return false;
	}
	</script>
<?php
}
?>